import React from 'react';
import GLOBALS from '../globals'
import style from './styles/theme_item.module.css'

export default class ThemeItem extends React.Component {
  constructor(props) {
    super(props);
  }

  changeVisualiser() {
      return this.props.callback(this.props.id)
  }
  
  render () {

    const getThumb = (id) => {
        const src = `${GLOBALS.scene_res}/${id}/${id}_thumb.jpg`;
        return src;
    };

    const id = this.props.id;
    const current = this.props.current; 
    const scenes= this.props.scenes

    const paint = {
        "title": scenes[id].title,
        "description": scenes[id].pseudo_description,
        "thumb": getThumb(id),
    }

    return (
      <div onClick={this.changeVisualiser.bind(this)} className={current ? style.item_active : style.item}>
          <img src={paint.thumb} className={style.cover}/>
          <div className={style.info}>
            <div className={style.title}>{paint.title}</div>
            <div className={style.description}>{paint.description}</div>
          </div>

      </div>
    )
  };
}