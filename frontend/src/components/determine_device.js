

function determineDevice() {
    const w = window.innerWidth;
    const h = window.innerHeight;
    const device = w > h ? "desktop" : "mobile";
    return device;
}

export default determineDevice;