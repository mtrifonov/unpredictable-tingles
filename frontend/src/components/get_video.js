
async function getVideo(videoUrl) {

    let videoBlob = await fetch(videoUrl).then(r => r.blob());
    let videoObjectUrl = URL.createObjectURL(videoBlob);
    let video = document.createElement("video");
    video.src = videoObjectUrl; 
    return video

  }
  
  export default getVideo;