import React from 'react';
import GLOBALS from '../globals'
import Visualiser from './visualiser';
import Controller from './controller';
import Diplomat from './diplomat'
import getFrames from './get_frames'
import processAnalysis from './process_analysis'
import SCENES from './scenes/scenes'

export default class Running extends React.Component {
  constructor(props) {
      // Receives only session id. Initiates running lifecycle.
    super(props);
    this.session_id = this.props.session_id;
    this.play_object = null;
    this.scene_resources = null;
    this.analysis = null;
    this.diplomat = new Diplomat();
    this.pA = new processAnalysis();
    this.state = {
        "play_label": "loading",
        "visualiser": "loading",
        "quality": "none",
        "id": "loading:loading:none",
        "playback": false,
    };
    this.scene_id = "ec";
    this.quality_level = 0;
    this.permission_key = "ec";
  }

  componentDidMount() {
      // Initialize Playback&Sync Loop
      this.checkPlayback(true);
      this.loadResources(this.permission_key);
      const w = window.innerWidth;
      const h = window.innerHeight;
      this.desktop = w > h ? true : false;
  };

  async componentDidUpdate() {
      if (this.state.playback === true) {
        await this.prepAnalysis();
        this.setState({
            "play_label": this.state.play_label,
            "visualiser": this.state.visualiser,
            "quality": this.state.quality,
            "id": this.state.id,
            "playback": "ready",           
        })
      }
  };

  componentWillUnmount() {
  };

  improveQuality(key) {
    let potential = this.quality_level < GLOBALS["quality_levels"].length - 1 ? true : false;
    if (this.desktop === false) {
        potential = GLOBALS["quality_levels"][this.quality_level].name == "500" ? false : true;
    }
    if (potential) {
        this.quality_level += 1;
        this.loadResources(key);
    }
    else {
        console.log("Maximum Quality Reached")
    }
  }

  async loadResources(key) {
        const scene_resources = await getFrames(SCENES[this.scene_id],this.quality_level);

        if (key == this.permission_key) {
            this.scene_resources = scene_resources
            this.current_quality_level = this.quality_level;
            if(this.suggestState()) {
                this.improveQuality(key)
            };
            return;
        }
        console.log("Permission denied " +key)

  };

  async prepAnalysis() {
      // Will have to add error handling here as well eventually
    const raw = await this.diplomat.getAnalysis(this.session_id,this.play_object.item.id);
    this.analysis_object = this.pA.process(raw);
    return;
  };

  checkPlayback(running) {
    const refresh_ms = 2000;
    setTimeout( async() => {
        this.play_object = await this.diplomat.getPlay(this.session_id);
        this.suggestState();
        this.checkPlayback(true);
        return;
    },refresh_ms);
  };

  changeVisualiser(id) {
      this.scene_id = id;
      this.scene_resources = null;
      this.quality_level = 0;
      this.permission_key = id;
      this.loadResources(id);
      this.suggestState();
  }

  suggestState() {
    // Events, simplified:
    // loading
    // no_session
    // song_paused
    // load_playback
    // error_occurred

    let suggested_state = {
        "play_label": null,
        "visualiser": null,
        "quality": null,
        "id": null,
        "playback": true,
    }
    const process_suggested_state = ((play_object,scene_resources) => {
        // Interpret Play Object Data | Mutually Exclusive
        if (play_object != null) {
            if (play_object === "no_session") {
                suggested_state.play_label = "no_session";
                suggested_state.playback = false;
            }
            else if (play_object.is_playing) {
                suggested_state.play_label = play_object.item.id;
            }
            else if (play_object.is_playing === false) {
                suggested_state.play_label = "song_paused";
                suggested_state.playback = false;
            }
            else {
                suggested_state.play_label = "error_occurred";
                suggested_state.playback = false;
            };
        }
        else {
            suggested_state.play_label = "loading";
            suggested_state.playback = false;
        };
        // Interpret Scene Resources Data
        if (scene_resources === null) {
            suggested_state.visualiser = "loading";
            suggested_state.quality = "none";
            suggested_state.playback = false;
        }
        else {
            suggested_state.visualiser = this.scene_id;
            suggested_state.quality = this.current_quality_level;
        };
    })(this.play_object,this.scene_resources);

    suggested_state.id = `${suggested_state.play_label}:${suggested_state.visualiser}:${suggested_state.quality}`;
    const should_update = suggested_state.id != this.state.id ? true : false;
    if (should_update) {
        console.log(suggested_state)
        console.log(this.scene_resources)
        this.setState(suggested_state)
        return true
    };
    return false;
  };

  render () {
    if (this.state.playback === "ready") {
        return (
            <div>
                <Controller content={{
                    "state": this.state,
                    "play_object": this.play_object,
                    "scenes": SCENES,
                    "active_scene": this.scene_id,
                    "quality_level": this.current_quality_level,
                    "callback": this.changeVisualiser.bind(this)
                    }} />
                <Visualiser content={{
                    "play_object": this.play_object,
                    "analysis_object": this.analysis_object,
                    "scene_resources": this.scene_resources,
                    "quality_level": this.current_quality_level,
                    "scene_blueprint": SCENES[this.scene_id]
                    }} />
            </div>
        )
    }
    return (
        <Controller content={{
            "state": this.state,
            "play_object": this.play_object,
            "scenes": null,
            "active_scene": null
            }} />
    )
  };
};