
["1","2","3","4","5","6"]
["2","3","4","5","6"]
["3","4","5","6"]
["4","5","6"]
["5","6"]
["6"]


["1","2","3","4"]
["2","3","4"]
["3","4"]
["4"]

function match_rythm(rythm,index) {
    const match_rythm_it = (rythm,index,mod,array) => {
        const index_temp = Math.floor(index / mod);
        mod = mod * rythm.length;
        const beat = index_temp % rythm.length
        array.push(rythm[beat].id)
        if (rythm[beat].branch == "rythm") {
            return match_rythm_it(rythm[beat].rythm,index,mod,array)
        };
        return [rythm[beat],array];

    };
    return match_rythm_it(rythm,index, 1,[])
};

const test = {
    "id": "quiet",
    "value": 0.2,
    "branch": "rythm",
    "rythm": [
        {
            "id": "all",
            "branch": "rythm",
            "rythm": [
                {"id": "1","branch": null},
                {"id": "2","branch": null},
                {"id": "3","branch": null},
                {"id": "4","branch": null},
            ],
        },
        {
            "id": "all2",
            "branch": "rythm",
            "rythm": [
                {"id": "1","branch": null},
                {"id": "2","branch": null},
                {"id": "3","branch": null},
                {"id": "4","branch": null},
            ],
        },
    ],
}


const arrr = () => {
    let arr = [];
    for (var i = 0; i < 10; i++) {
        const leafstr = match_rythm(test.rythm,i);
        console.log(leafstr[1].join(' '))
    };
    return arr
};

arrr()


