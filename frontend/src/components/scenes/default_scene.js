import moLib from '../motion_library'
import ryLib from '../rythm_library'

const ml = new moLib();
const rl = new ryLib();


// defines composition rythms for 3 energy levels


const genComp = (comp,bar,f) => {
    const template = {
        "id": "tangerine",
        "scale": (f) => null,
        "opacity": (f) => null,
        "position": (f) => null,
        "state": (f) => null,
        "store": (f) => null,
        "hide": (f) => null,
        "rotate": (f) => null,
    },
    return;

};

const genInstances = (f) => {

    const current_bar = f.bar.counter[0];
    //const bars_length = f.bar.counter[1]; // this might be useless after all;

    // Step 1: Pick which energy level we're at.
    const loudness = f.segment.current.sigmoid;
    const rythm = (() => {
        for (var i = 0; i<levels.length; i++) {
            if(loudness <= levels[i].loudness) {
                return levels[i].rythm;
            }
        }
    })();
    const current_comp = rl.match_rythm(rythm,current_bar);

    const instances = genComp(current_comp,current_bar,f);

    return instances;

};


const test = {

    "id": "test",
    "title": "Testing Purposes Only",
    "description": "A dry '69 visualiser.",
    "vistruments": [
        {
            "id": "tangerine",
            "dimensions": [500,500],
            "count": 49,
            "mask": true
        }
    ],
    "engine": "custom_instances",
    "instances": (f) => genInstances(),
}

export default test;