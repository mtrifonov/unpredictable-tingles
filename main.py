from google.cloud import firestore
from flask import Flask,redirect,request,render_template,sessions,url_for
import json
import base64
import requests
import random
import string


'''
GLOBALS = {
    "base_url": "https://tingles.wtf"
}

'''
GLOBALS = {
    "base_url": "http://localhost"
}


db = firestore.Client()
session_store = db.collection(u'sessions')



with open('secret.json', 'r') as f:
    SECRET = json.load(f)
    CLIENT_ID = SECRET["id"]
    CLIENT_SECRET = SECRET["secret"]

app = Flask(__name__)


@app.route('/test')
def test():
    return json.dumps({"you have": "passed the test"})

@app.route('/callback', methods=['GET'])
def callback():
    # RECEIVE AUTH CODE
    # SAVE ACCESS & REFRESH TOKEN AS SESSION
    # REDIRECT TO CLIENT WITH SESSION KEY
    def getTokens(code):
        url = "https://accounts.spotify.com/api/token"
        data = {
            "grant_type": "authorization_code",
            "redirect_uri": "{}/callback".format(GLOBALS["base_url"]),
            "code": code,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET
        }
        return requests.post(
            url,
            data=data
        )
    def random_string(N):
        return ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=N))

    code = request.args.get("code")
    tokens = getTokens(code).json()

    if checkError(tokens):
        return json.dumps(tokens)
    session_id = random_string(144)
    entry = {
        u'access': tokens["access_token"],
        u'refresh': tokens["refresh_token"]
    }
    document = session_store.document(session_id)
    document.set(entry)
    location = "{}/?session_id={}".format(GLOBALS["base_url"],session_id)
    return redirect(location)


@app.route('/access', methods=['GET'])
def getAccess():
    session_id = request.args.get("session_id")
    access_code = session_store.document(session_id).get().to_dict()["access"]
    return json.dumps({"access": access_code})

@app.route('/refresh', methods=['GET'])
def refreshAccess():
    def helper(refresh_code):
        url = "https://accounts.spotify.com/api/token"
        data = {
            "grant_type": "refresh_token",
            "refresh_token": refresh_code,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET
        }
        return requests.post(
            url,
            data=data
        )
    session_id = request.args.get("session_id")
    refresh_code = session_store.document(session_id).get().to_dict()["refresh"]

    response = helper(refresh_code).json()
    if checkError(response):
        return json.dumps({"error": "an unknown error has occurred"})

    session_store.document(session_id).set({
        u'access': response["access_token"],
        u'refresh': refresh_code
    })
    return json.dumps({"access": response["access_token"]})


@app.route('/play', methods=['GET'])
def play(session_id=None):
    # RECEIVE SESSION KEY
    # MAKE API CALL WITH ACCESS CODE
    # IF INVALID, REQUEST NEW ACCESS CODE
    # PROCESS AND RETURN API SONG DATA
    def getResponse(access_code):
        url = "https://api.spotify.com/v1/me/player/currently-playing"
        headers = {
            "Authorization": "Bearer {}".format(access_code)
        }
        response = requests.get(url, headers=headers)
        if response.status_code == 204:
            return {
                "active_session": False
            }
        return response.json()

    def getSong(response):
        song = {
            "active_session": True,
            "is_playing": response["is_playing"],
            "timestamp": response["timestamp"],
            "progress": response["progress_ms"],
            "id": response["item"]["id"],
            "name": response["item"]["name"]
        }
        return song

    if session_id == None:
        session_id = request.args.get("session_id")
    access_code = session_store.document(session_id).get().to_dict()["access"]
    response = getResponse(access_code)
    if accessExpired(response):
        return refreshPlay(session_id)
    if checkError(response):
        return json.dumps(response)
    if "active_session" in response:
        return json.dumps(response)
    song = getSong(response)
    return json.dumps(song)


@app.route('/analysis', methods=['GET'])
def analysis():
    # RECEIVE SESSION KEY
    # MAKE API CALL WITH ACCESS CODE
    # IF INVALID, REQUEST NEW ACCESS CODE
    # PROCESS AND RETURN API SONG ANALYSIS
    def getResponse(id,access_code):
        url = "https://api.spotify.com/v1/audio-analysis/{}".format(id)
        headers = {
            "Authorization": "Bearer {}".format(access_code)
        }
        response = requests.get(url, headers=headers)
        return response.json()

    def getAnalysis(response):
        return pA.processAnalysis(response)

    session_id = request.args.get("session_id")
    song = json.loads(play(session_id))
    id = song["id"]
    access_code = session_store.document(session_id).get().to_dict()["access"]
    response = getResponse(id,access_code)
    if checkError(response):
        return json.dumps(response)
    analysis = getAnalysis(response)
    return json.dumps({
        "song": song,
        "analysis": analysis
    })



def refreshPlay(session_id):
    # It might be that I need to figure out headers here.
    def helper(refresh_code):
        url = "https://accounts.spotify.com/api/token"
        data = {
            "grant_type": "refresh_token",
            "refresh_token": refresh_code,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET
        }
        return requests.post(
            url,
            data=data
        )

    refresh_code = session_store.document(session_id).get().to_dict()["refresh"]
    response = helper(refresh_code).json()
    if checkError(response):
        return json.dumps({"error": "an unknown error has occurred"})

    session_store.document(session_id).set({
        u'access': response["access_token"],
        u'refresh': refresh_code
    })
    return play(session_id)


def checkError(response):
    if "error" in response:
        return True
    return False

def accessExpired(response):
    if "error" in response:
        if response["error"]["message"] == "The access token expired":
            return True
    return False

if __name__ == '__main__':
    app.run()
